﻿using System.Xml.Linq;

namespace Searcher
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string answer = "Y";
            string[] items = { "Sausage Breakfast Taco",
                                "Potato and Egg Breakfast Taco", "Sausage and Egg Biscuit",
                                "Bacon and Egg Biscuit", "Pancakes"};
            decimal[] prices = { 3.99M, 3.29M, 3.70M, 3.99M, 4.79M };
            decimal? itemPrice = null;


/*
            Console.WriteLine("Enter another order? y/n");
            answer = Console.ReadLine().ToUpper();*/

            do
            {
                Console.WriteLine("Select an item to order: ");
                Console.WriteLine();
                Console.WriteLine("1 - Sausage Breakfast Taco");
                Console.WriteLine();
                Console.WriteLine("2 - Potato and Egg Breakfast Taco");
                Console.WriteLine();
                Console.WriteLine("3 - Sausage and Egg Biscuit");
                Console.WriteLine();
                Console.WriteLine("4 - Bacon and Egg Biscuit");
                Console.WriteLine();
                Console.WriteLine("5 - Pancakes");
                string orderItem = Console.ReadLine();

                itemPrice = null;
                itemPrice = GetPrice(items, prices, orderItem);
                if (itemPrice.HasValue)
                {
                    Console.WriteLine($"You ordered: {orderItem} at {itemPrice}");
                }
                Console.WriteLine("Enter another order? y/n");
                try
                {
                    answer = Console.ReadLine().ToUpper();
                }
                catch (Exception e)
                {
                    Console.WriteLine("Invalid Input. Error:" + e);
                    return;
                }
                if (answer != "N" && answer != "Y")
                {
                    Console.WriteLine("Enter Y or N");
                    answer = Console.ReadLine().ToUpper();
                }
                if (answer == "N")
                {
                    break;
                }
            } while (answer == "Y");


        }

        public static decimal? GetPrice(string[] items, decimal[] prices, string orderItem)
        {
            /*            decimal? priceIndex = 0;*/

            int priceIndex = 0;

            for (int i = 0; i < items.Length; i++)
            {
                if (items[i] == orderItem)
                {
                    priceIndex = i;
                    break;
                }
            }

            if (priceIndex == -1)
            {
                Console.WriteLine("Item not found");
                return null;
            }
            else
            {
                return prices[priceIndex];
            }

        }
    }
}